//
//  textView.swift
//  ios2020_swiftUI
//
//  Created by chikuma on 2020/6/1.
//  Copyright © 2020 Verniy"Realive"Akatsuki. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

struct TextView: UIViewRepresentable {
    func makeUIView(context: UIViewRepresentableContext<TextView>) -> UITextView {
        let textView = UITextView()
        textView.text = "TYPE SOMETHING"
        textView.frame = CGRect(x: 0, y: 0, width: 250, height: 200)
        textView.font = UIFont(name: "Helvetica-Light", size: 25)
        textView.textAlignment = .center
        textView.isEditable = true
        return textView
    }
    func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<TextView>) {
        
    }
    typealias UIViewType = UITextView
}

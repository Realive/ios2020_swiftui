//
//  activityController.swift
//  ios2020_swiftUI
//
//  Created by chikuma on 2020/6/1.
//  Copyright © 2020 Verniy"Realive"Akatsuki. All rights reserved.
//
import Foundation
import SwiftUI

import SafariServices

struct SafariView: UIViewControllerRepresentable{
    let url: URL
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<SafariView>) -> SFSafariViewController {
        SFSafariViewController(url: url)
    }
    
    func updateUIViewController(_ uiViewController: SFSafariViewController, context: UIViewControllerRepresentableContext<SafariView>) {
        
    }
    
    typealias UIViewControllerType = SFSafariViewController
}

struct ActivityController: UIViewControllerRepresentable{
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ActivityController>) -> UIActivityViewController {
        let controller = UIActivityViewController(activityItems: ["komica", UIImage(named:"001")!, URL(string:"http://komica.org/m/")!], applicationActivities: nil)
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIActivityViewController, context: UIViewControllerRepresentableContext<ActivityController>) {
        
    }
    typealias UIViewControllerType = UIActivityViewController
}

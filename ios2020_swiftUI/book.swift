//
//  book.swift
//  ios2020_swiftUI
//
//  Created by chikuma on 2020/5/9.
//  Copyright © 2020 Verniy"Realive"Akatsuki. All rights reserved.
//

import Foundation

struct Book {
    var name: String = ""
    var author: String = ""
    var img: String = ""
    var intro: String = ""
    
    init(name: String, author: String, img: String, intro: String) {
        self.name = name
        self.author = author
        self.img = img
        self.intro = intro
    }
}

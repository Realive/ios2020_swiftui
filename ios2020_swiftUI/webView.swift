//
//  webView.swift
//  ios2020_swiftUI
//
//  Created by chikuma on 2020/6/1.
//  Copyright © 2020 Verniy"Realive"Akatsuki. All rights reserved.
//

import Foundation
import SwiftUI
import WebKit

struct WebView: UIViewRepresentable {
    let urlStr: String
    
    func makeUIView(context: Context) -> WKWebView {
        let webView = WKWebView()
        if let url = URL(string: urlStr) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        return webView
    }
    
    func updateUIView(_ uiView: WKWebView, context: UIViewRepresentableContext<WebView>) {

    }
    typealias UIViewType = WKWebView
}

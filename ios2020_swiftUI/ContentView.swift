//
//  ContentView.swift
//  ios2020_swiftUI
//
//  Created by chikuma on 2020/5/9.
//  Copyright © 2020 Verniy"Realive"Akatsuki. All rights reserved.
//

import SwiftUI
import Foundation

struct ContentView: View {
    /*
    @State private var posts = [Post]()
    func fetchPosts() {
        let urlStr = "https://www.dcard.tw/_api/posts?popular=true"
        if let url = URL(string: urlStr) {
            URLSession.shared.dataTask(with: url) {
                (data, response, error) in
                let decoder = JSONDecoder()
                if let data = data{
                    do {
                        let posts = try decoder.decode([Post].self, from: data)
                        self.posts = posts
                    } catch {
                        print(error)
                    }
                }
            }.resume()
        }
    }
 */
    @State private var showPage = false
    @State private var sharePage = false
    var body: some View {
        VStack {
            //WebView(urlStr: "http://komica.org/m/")
            Button("show page") {
                self.showPage = true
            }.sheet(isPresented: $showPage) {
                SafariView(url: URL(string:"http://komica.org/m/")!)
            }
            
            Button("share") {
                self.sharePage = true
            }.sheet(isPresented: $sharePage) {
                ActivityController()
            }
            TextView()
        }
        /*
        NavigationView {
            VStack {
                NetworkImage(urlStr: "https://i.imgur.com/cqAklTF.jpg")
                    .scaledToFill()
                    .frame(width:200, height:200)
                    .clipShape(Circle())
            
                List(posts.indices,id: \.self, rowContent: {
                    (item) in NavigationLink(destination: PostView(id: self.posts[item].id)) {
                        PostRow(post: self.posts[item])
                    }
                    //(index) in PostRow(post: self.posts[index])
                })
                .onAppear {
                        self.fetchPosts()
                }
            }
        }.navigationBarTitle("D能card", displayMode: .inline)
 */
    }
}

struct PostRow: View {
    var post: Post
    var body: some View {
        HStack {
            if (!post.media.isEmpty) {
                NetworkImage(urlStr:post.media[0].url)
                .scaledToFill()
                .frame(width: 60, height: 60)
                .clipped()
            }
            
            VStack(alignment: .leading) {
                Text(post.title).bold()
                Text(post.excerpt).lineLimit(2)
            }
            
            VStack(alignment: .trailing) {
                Text(post.school ?? "Secret")
                Text(String(format:"❤︎:%d", post.likeCount))
                Text(String(format:"✑:%d", post.commentCount))
            }
        }
    }
}

struct PostView: View {
    init (id: Int) {
        self.id = id
    }
    
    var id: Int
    @State private var title: String = "?????"
    @State private var content: String = ""
    //@State private var specPost: SpecPost?
    var body: some View {
        VStack {
            Text(self.title).bold()
            Text(self.content)
        }
        .navigationBarTitle("Post", displayMode: .inline)
        .onAppear {
            self.fetchPosts()
        }
    }
    
    func fetchPosts() {
        if let url = URL(string: "https://www.dcard.tw/_api/posts/\(id)") {
            //print(String(format:"%s%d",urlStr, id))
            URLSession.shared.dataTask(with: url) {
                (data, response, error) in
                let decoder = JSONDecoder()
                if let data = data{
                    do {
                        let post = try decoder.decode(SpecPost.self, from: data)
                        self.title = post.title
                        print(post.title)
                        self.content = post.content
                    } catch {
                        print(error)
                    }
                }
            }.resume()
        }
        print(String("https://www.dcard.tw/_api/posts/\(id)"))
    }
}

struct NetworkImage: View {
    var urlStr: String
    @State private var image = Image(systemName: "photo")
    @State private var dlImageOk = false
    
    func download() {
        if let url = URL(string: urlStr), let data = try? Data(contentsOf: url), let uiImage = UIImage(data: data) {
            self.image = Image(uiImage: uiImage)
            self.dlImageOk = true
        }
    }
    var body: some View{
        image
            .resizable()
            .onAppear{
                if self.dlImageOk == false {
                    self.download()
                }
            }
    }
}

struct Post: Codable {
    var id: Int
    var title: String
    var excerpt: String
    var school: String?
    var likeCount: Int
    var commentCount: Int
    var media: [Media]
}

struct Media: Codable {
    var url: String
}

struct SpecPost: Codable {
    var id: Int
    var title: String
    var content: String
}
    /*
    @State private var moveDistance: CGFloat = 0
    @State private var opacity: Double = 1
    
    var body: some View {
        NavigationView {
            VStack {
                Button("kansaiki") {
                    self.moveDistance -= 50
                    self.opacity -= 0.1
                }
                .font(.title)
                NavigationLink(destination: TakoView()) {
                    VStack {
                        Image("001").resizable().renderingMode(.original).scaledToFit().frame(width: 200, height: 200).offset(x: moveDistance, y: 0).opacity(opacity).animation(.default)
                    }
                
                }
                
           
            }
            .navigationBarTitle("Kansaiki", displayMode: .inline)
        }
    }
}

struct TakoView: View {
    var body: some View {
        VStack {
            Text("Tako")
        Image("002").resizable().scaledToFit().frame(width: 200, height: 200)
            .navigationBarTitle("Tako", displayMode: .inline)
        }
    }
}
*/
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
